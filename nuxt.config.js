export default {
  mode: 'spa',

  head: {
    title: process.env.npm_package_author_name || '',

    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'description', content: process.env.npm_package_description || '', hid: 'description' },
      { name: 'author', content: process.env.npm_package_author_name || '' }
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: {
    color: '#fff'
  },

  css: [
    //
  ],

  plugins: [
    //
  ],

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss'
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/svg'
  ],

  axios: {
    //
  },

  build: {
    extend(config, ctx) {
      //
    }
  }
}
